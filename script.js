const vh = window.innerHeight;
$(function () {
	$(window).scroll(function () {
		if ($(this).scrollTop() > vh) {
			$("#toTop").fadeIn();
		} else {
			$("#toTop").fadeOut();
		}
	});
	$("#toTop").click(function () {
		$("body,html").animate({ scrollTop: 0 }, window);
	});
});

$(".btn-news").click(function () {
	$(".posts").slideToggle(1000);
});

$(".ref-on-page a").on("click", function () {
	let href = $(this).attr("href");

	$("html, body").animate(
		{
			scrollTop: $(href).offset().top,
		},
		{
			duration: 370,
			easing: "linear",
		}
	);
	return false;
});
